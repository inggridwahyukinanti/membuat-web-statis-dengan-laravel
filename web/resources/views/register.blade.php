<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hari 1-Berlatih HTML</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2> 
    <form action="/welcome" method="POST">
        @csrf 
        <label>First name:</label> <br>
        <input type="text" name="firstnama"> <br>
        <br> <br>
        <label>Last name:</label> <br>
        <input type="text" name="lastname"> <br>
        <br>
        <label>Gender</label> <br>
        <input type="radio" name="Male">Male <br>
        <input type="radio" name="Female">Female <br>
        <input type="radio" name="Other">Other <br>
        <br>
        <label>Nationality:</label> <br>
        <select name="nationality">
            <option value="Indonesia">Indonesia</option> <br>
            <option value="Malaysia">Malaysia</option> <br>
            <option value="Singapore">Singapore</option> <br>
            <option value="Thailand">Thailand</option> <br>
            <option value="Other">Other</option> <br>
        </select> <br> <br>
        <label>Language Spoken:</label> <br>
        <input type="checkbox" name="Bahasa Indonesia"> Bahasa indonesia <br>
        <input type="checkbox" name="English"> English <br>
        <input type="checkbox" name="Other">Other <br>
        <br>
        <label>Bio:</label> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <br>

        <input type="submit">
    </form>


</body>
</html>